import { Route, Switch,Link } from 'react-router-dom';
import './App.css';

import AllMeetupsPage from './pages/AllMeetups';
import NewMeetupPage from './pages/NewMeetup';
import FavoritesPage from './pages/Favorites';

function App() {
  return (
    <div>
      <ul>
        <li><Link to="/">All Meetups</Link></li>
        <li><Link to="/new-meetup">New Meetup</Link></li>
        <li><Link to="/favorites">Favorites</Link></li>
      </ul>
      <Switch>
        <Route path='/' exact>
          <AllMeetupsPage />
        </Route>
        <Route path='/new-meetup'>
          <NewMeetupPage />
        </Route>
        <Route path='/favorites'>
          <FavoritesPage />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
